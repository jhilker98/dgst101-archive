#+title: A Response to Bradbury's 'There Will Come Soft Rains'
#+date: 2021-01-30 19:31:01
#+layout: single
#+type: post
#+draft: false
#+summary: My personal thoughts on Ray Bradbury's 'There Will Come Soft Rains'.
#+categories[]: dgst101
#+tags[]: dgst101

[[/pics/dgst101/soft-rain.jpg][]]
I personally feel like /'There Will Come Soft Rains'/ focuses on how technology is a danger to people and really on the negative aspects as a whole, rather than any of the positive aspects that technology provides people. To me, it sort of comes across as a "technology bad" sort of tone, which kind of seems condescending (at least, it does in my opinion).

I am a cybersecurity minor, so I definitely understand how technology and poorly implemented tools/APIs make life much more secure. (On a side note - some political stuff ahead. Not any real opinions, but consider this a heads-up.) As an example, before Parler was brought offline after the events from the 6th, security researchers were able to gather over 30TB of data due to a poorly implemented API[fn:1]. And in addition, many major technology companies don't exactly respect a user's privacy - namely Google, Facebook, and Amazon, though many others don't respect privacy as well. Plus, it seems like some new attack is happening incredibly frequently.

However, I also feel like technology has also been proven to be a helpful resource. With the pandemic, online conferencing tools like Zoom have exploded in popularity, and games like /Animal Crossing/ and /Among Us/ have allowed people to experience some form of escapism from the pandemic. 

Overall, I feel like Bradbury was saying that even though we have all this technology and all these tools that go along with it, it's not the same as experiencing the same thing in person. I absolutely agree with that sentiment, but I also feel like he explpained that with a condescending tone that I personally found off-putting.





* References
[fn:1] https://www.usatoday.com/story/tech/news/2021/01/11/parler-hack-platform-archived-hackers-capitol-riots/6629772002/